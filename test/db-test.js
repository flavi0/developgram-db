'use strict'

const test = require('ava')
const Db = require('../lib/db')
const fixtures = require('./fixtures')
const utils = require('../lib/utils')
const uuid = require('uuid-base62')
const r = require('rethinkdb')

// la configuracion de la bd se cargara para cada test
test.beforeEach('configurar base de datos', async t => {
  const dbName = `developgram_${uuid.v4()}`
  const db = new Db({ db: dbName, setud: true }) // configurar la bd
  // modificar el contexto
  /**
   * se usará un contexto para cada text para que no perduren datos
   * que podrían alterar a otros test
   */
  await db.connect()
  t.context.db = db
  t.context.dbName = dbName
  t.true(db.connected, 'debería estar conectada')
})

// crear una base de datos por cada test, cuando termine el test, la BD se limpiará
test.afterEach.always('desconexion y limpiar la BD', async t => {
  const db = t.context.db
  const dbName = t.context.dbName
  await db.disconnect()
  t.false(db.connected, 'debería no estar conectado')
  // borrar la bd
  const conn = await r.connect({})
  await r.dbDrop(dbName).run(conn)
})

/*
test.after('desconectar de la base de datos', async t => {
  await db.disconnect()
  t.false(db.connected, 'debería no estar conectado')
})

// siempre se ejecutara
test.after.always('borrar base de datos', async t => {
  const conn = await r.connect({})
  await r.dbDrop(dbName).run(conn)
})
*/

test('guardar imagen', async t => {
  // console.log('test-db')
  const db = t.context.db
  t.is(typeof db.saveImage, 'function', 'saveImage es una función')

  const image = fixtures.getImage()
  const created = await db.saveImage(image)
  t.is(created.description, image.description)
  t.is(created.url, image.url) // mantener la misma url de la imagen luego de guadar la imagen
  t.is(created.likes, image.likes, 'likes-comp')
  t.is(created.liked, image.liked, 'liked-comp')
  t.deepEqual(created.tags, ['test', 'developgram', 'tdd', 'pentesting', 'xd'], 'tag-test')
  t.is(created.userId, image.userId, 'userId-comp')
  t.is(typeof created.id, 'string')
  t.is(created.publicId, uuid.encode(created.id)) // el id publico es igual a la codificacion del id de la imagen
  t.truthy(created.createdAt, 'fecha-creacion') // garantizar que venga con la fecha de creacion
})

test('like imagen', async t => {
  const db = t.context.db
  t.is(typeof db.likeImage, 'function', 'likeImage debe ser una funcion')

  const image = fixtures.getImage()
  // guardar la imagen
  const created = await db.saveImage(image)
  // dar like a la imagen
  const result = await db.likeImage(created.publicId)
  t.true(result.liked, 'la imgagen retorna like')
  t.is(result.likes, image.likes + 1, 'likes de imagen más 1')
})

test('obtener imagen', async t => {
  const db = t.context.db
  t.is(typeof db.getImage, 'function', 'getImage es una funcion')
  const image = fixtures.getImage()
  const created = await db.saveImage(image)
  const result = await db.getImage(created.publicId)
  t.deepEqual(created, result, 'la imagen creada es la que se obtiene de la bd')
  t.deepEqual(await db.getImage('foo'), false)
})

test('listar imagenes', async t => {
  const db = t.context.db
  const images = fixtures.getImages(3)
  // arreglo de promesas que se resolveran con Promise all
  const saveImages = images.map(img => db.saveImage(img))
  const created = await Promise.all(saveImages)
  const result = await db.getImages()
  t.is(created.length, result.length, 'hay tantas imagenes consultadas como creadas')
})

test('guardar usuario', async t => {
  const db = t.context.db

  t.is(typeof db.saveUser, 'function', 'saveUser es una funcion')
  const user = fixtures.getUser()
  const passwordPlano = user.password
  const created = await db.saveUser(user)

  t.is(user.username, created.username)
  t.is(user.email, created.email)
  t.is(user.name, created.name)
  t.is(utils.encrypt(passwordPlano), created.password)
  t.is(typeof user.id, 'string')
  // truthy verifica que haya algun valor
  t.truthy(created.createdAt, 'tiene fecha')
})

test('obtener usuario', async t => {
  const db = t.context.db

  t.is(typeof db.getUser, 'function', 'getUser es una funcion')

  const user = fixtures.getUser()
  const created = await db.saveUser(user)
  const result = await db.getUser(user.username)

  t.deepEqual(created, result, 'usuario creado igual al de la base de datos')
  // t.throws(db.getUser('otro'), /not found/)
  t.deepEqual(await db.getUser('foo'), false) // en caso se busque a un usuario que no existe
})

test('autenticar usuario', async t => {
  const db = t.context.db
  t.is(typeof db.authenticate, 'function', 'aunthenticate es una funcion')
  const user = fixtures.getUser()
  const passwordPlano = user.password
  await db.saveUser(user)

  const success = await db.authenticate(user.username, passwordPlano)
  t.true(success, 'se autentico al usuario')
  // probando un fallo
  const fail = await db.authenticate(user.username, 'otro-password')
  t.false(fail, 'contraseña incorrecta, fallo correcto :v')

  const failure = await db.authenticate('foo', 'bar')
  t.false(failure, 'usuario y/o contraseña -> error')
})

test('listar imagenes por usuario', async t => {
  const db = t.context.db
  t.is(typeof db.getImagesByUser, 'function', 'getImagesByUser es una funcion')
  const images = fixtures.getImages(10)
  const userId = uuid.uuid()
  const random = Math.round(Math.random() * images.length)
  const saveImage = []
  for (let i = 0; i < images.length; i++) {
    // asinando aletaoriamente unas imagenes al usuario
    if (i < random) {
      images[i].userId = userId
    }
    saveImage.push(db.saveImage(images[i]))
  }
  // guardando en la bd
  await Promise.all(saveImage)

  const result = await db.getImagesByUser(userId)
  t.is(result.length, random, 'el usuario tiene tantas imagenes con el numero random generado')
})

test('listar imagenes por tag', async t => {
  const db = t.context.db
  t.is(typeof db.getImagesByTag, 'function', 'getImagesByTag es una funcion')
  const images = fixtures.getImages(10)
  const tag = '#filterit'
  const random = Math.round(Math.random() * images.length)
  const saveImage = []
  for (let i = 0; i < images.length; i++) {
    // asinando aletaoriamente unas imagenes al usuario
    if (i < random) {
      images[i].description = tag
    }
    saveImage.push(db.saveImage(images[i]))
  }
  // guardando en la bd
  await Promise.all(saveImage)

  const result = await db.getImagesByTag(tag)
  t.is(result.length, random, 'el usuario tiene tantas imagenes con el numero random generado')
})
