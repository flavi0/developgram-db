'use strict'

const test = require('ava')
const utils = require('../lib/utils')
const r = require('rethinkdb')

test('extrayendo hastags de texto', t => {
  // console.log('utils-file')
  let tags = utils.extractTags('a #picture with tags #AwEsOmE #AvA and #120 ##EjM')
  // asercion cuando se comparan objetos
  t.deepEqual(tags, [
    'picture',
    'awesome',
    'ava',
    '120',
    'ejm'
  ], 'test de Hashtags')

  tags = utils.extractTags('a picture with no tags')
  t.deepEqual(tags, [], 'testo sin hashtags')

  tags = utils.extractTags()
  t.deepEqual(tags, [], 'sin argumentos')

  tags = utils.extractTags(null)
  t.deepEqual(tags, [], 'en null')
})

r.db('developgram_77tJb9JlJlzeqI4fa8HIkf').table('imagenes').insert({
  url: 'http://developgram.test/imagen.jpg'
})
/*
test('encriptar contraseña', t => {
  const password = 'ejm123'
  const encriptada = '41bff16c16f4fc3c9fd638d41cd85c2bb88562cebd30152eec4a85fa4b9f6e49'
  const result = utils.encrypt(password)
  t.is(result, encriptada, 'encriptacion igual a resultado')
})
*/
