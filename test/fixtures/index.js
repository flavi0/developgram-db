'use strict'

const uuid = require('uuid-base62')

const fixtures = {
  getImage () {
    return {
      description: 'descripcion de #test en #developgram #tdd #pentesting #XD',
      url: `https://developgram.test/${uuid.v4()}.jpg`,
      likes: 0,
      liked: false,
      userId: uuid.v4()
    }
  },
  getImages (n) {
    const images = []
    while (n-- > 0) {
      images.push(this.getImage())
    }
    return images
  },
  getUser () {
    return {
      name: 'RandomName',
      username: `user_${uuid.v4()}`,
      password: uuid.uuid(),
      email: `${uuid.v4()}@developgram.test`
    }
  }
}

module.exports = fixtures
