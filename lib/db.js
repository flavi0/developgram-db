'use strict'

const r = require('rethinkdb')
const co = require('co')
const Promise = require('bluebird') // sobreescribiendo la clase nativa 'Promise' de JS
const uuid = require('uuid-base62')
const utils = require('./utils')

const defaults = {
  host: 'localhost',
  port: 28015,
  db: 'developgram'
}
class Db {
  constructor (options) {
    console.log('db2--init--> ', options, 'type-> ', typeof(options))
    options = options || {}
    this.host = options.host || defaults.host
    this.port = options.port || defaults.port
    this.db = options.db || defaults.db
    this.connected = false
    this.setup
    // this.setup = options.setup || false
     if (!options.setup) {
      console.log('NO-setup--options\n\n')
      this.setup = true
    } else {
      console.log('setup--existe\n')
      this.setup = options.setup
    }
  }

  connect (callback) {
    // objeto de conexion
    console.log("BD-Connect")
    this.connection = r.connect({
      host: this.host,
      port: this.port
    })

    this.connected = true

    const db = this.db
    const connection = this.connection

    if (!this.setup) {
      console.log('no-setup', this.setup)
      return Promise.resolve(connection).asCallback(callback)
    }
    console.log('SI-setup')
    // serie de correcion: función generadora que retornara
    // una promesa(el valor de setup) (parecido a async-await)
    const setup = co.wrap(function * () {
      // la promesa se resuelve con 'yield'
      const conn = yield connection
      // crear base de datos si no existe
      const dbList = yield r.dbList().run(conn)
      // console.log('dbList-index --> ', dbList.indexOf(db))
      if (dbList.indexOf(db) === -1) {
        // console.log('creando Base de Datos')
        // la base de datos no está creada, entonces crear:
        yield r.dbCreate(db).run(conn)

        // crear las tablas si no existe
        const dbTables = yield r.db(db).tableList().run(conn)
        if (dbTables.indexOf('imagenes') === -1) {
          try {
            yield r.db(db).tableCreate('imagenes').run(conn)
            // creando indice por el campo createdAt
            yield r.db(db).table('imagenes').indexCreate('createdAt').run(conn)
            // console.log('creando Tablas-imagenes')
            // permitir varios registros con el mismo indice
            yield r.db(db).table('imagenes').indexCreate('userId', { multi: true }).run(conn)
          } catch (error) {
            // console.log('no se creo imagenes-catch')
          }
        } else {
          // console.log('no se creo imagenes')
        }
        if (dbTables.indexOf('users') === -1) {
          try {
            yield r.db(db).tableCreate('users').run(conn)
            // console.log('creando tabla-users')
          } catch (error) {
            // console.log('no se creo users-catch')
          }
        } else {
          // console.log('no se creo users')
        }
      } else {
        // console.log('no se creo base de datos >>>>>>>>>>>>>>')
      }
      return conn
    })
    // método híbrido
    // Si no pasan 'callback' se retorna la promesa
    // Si pasan 'callback', se maneja 'setup' como función con callback
    return Promise.resolve(setup()).asCallback(callback)
  }

  disconnect (callback) {
    if (!this.connected) {
      return Promise.reject(new Error('No conectado')).asCallback(callback)
    }

    this.connected = false
    // resolver la conexion y cerrarla
    return Promise.resolve(this.connection).then(conn => conn.close())
  }

  saveImage (image, callback) {
    if (!this.connected) {
      return Promise.reject(new Error('No conectado')).asCallback(callback)
    }
    // referencia de conexion y db para la corutina
    const connectionRef = this.connection
    const dbRef = this.db
    // usando corutina(recibe un funcion constructora o generadora) de tareas
    const tasks = co.wrap(function * () {
      const conn = yield connectionRef
      image.createdAt = new Date()
      image.tags = utils.extractTags(image.description)
      // almacenar en la bd
      const result = yield r.db(dbRef).table('imagenes').insert(image).run(conn)

      // si hay algun error
      if (result.error > 0) {
        return Promise.reject(new Error(result.first_error))
      }
      // se asigna la posicion 0 porque se inserta de uno en uno
      image.id = result.generated_keys[0]

      // actualizando datos usando una corutina
      yield r.db(dbRef).table('imagenes').get(image.id).update({
        publicId: uuid.encode(image.id)
      }).run(conn)

      const created = yield r.db(dbRef).table('imagenes').get(image.id).run(conn)

      return Promise.resolve(created) // se resuelve el ultimo objeto creado
    })

    // resolviendo corutina
    return Promise.resolve(tasks()).asCallback(callback)
  }

  likeImage (id, callback) {
    if (!this.connected) {
      return Promise.reject(new Error('No conectado')).asCallback(callback)
    }
    // referencia de conexion y db para la corutina
    const connectionRef = this.connection
    const dbRef = this.db
    const getImage = this.getImage.bind(this)
    // usando corutina(recibe un funcion constructora o generadora) de tareas
    const tasks = co.wrap(function * () {
      const conn = yield connectionRef

      const image = yield getImage(id)
      // actualizar los valores
      yield r.db(dbRef).table('imagenes').get(image.id).update({
        liked: true,
        likes: image.likes + 1
      }).run(conn)
      // obteniendo la imagen final
      const created = yield getImage(id)
      return Promise.resolve(created)
    })
    // resolviendo corutina
    return Promise.resolve(tasks()).asCallback(callback)
  }

  getImage (id, callback) {
    if (!this.connected) {
      return Promise.reject(new Error('No conectado')).asCallback(callback)
    }
    // referencia de conexion y db para la corutina
    const connectionRef = this.connection
    const dbRef = this.db
    const imageId = uuid.decode(id) // decodificando en base al id
    // usando corutina(recibe un funcion constructora o generadora) de tareas
    const tasks = co.wrap(function * () {
      const conn = yield connectionRef
      const image = yield r.db(dbRef).table('imagenes').get(imageId).run(conn)
      if (!image) {
        return Promise.resolve(false)
      }
      return Promise.resolve(image)
    })
    // resolviendo corutina
    return Promise.resolve(tasks()).asCallback(callback)
  }

  getImages (callback) {
    if (!this.connected) {
      return Promise.reject(new Error('No conectado')).asCallback(callback)
    }
    // referencia de conexion y db para la corutina
    const connectionRef = this.connection
    const dbRef = this.db
    // usando corutina(recibe un funcion constructora o generadora) de tareas
    let tasks = null
    try {
      console.log('............xDDDD. .......')
      tasks = co.wrap(function * () {
        console.log('----function-tasks')
        const conn = yield connectionRef
        // yield r.db(db).table('imagenes').indexWait().run(conn)
        const images = yield r.db(dbRef).table('imagenes').orderBy(
          // index: r.desc('createdAt')
          r.desc('createdAt')).run(conn)
        console.log('----function-tasks-images--> ', images)
        // usando el API, images retorna un cursor (objeto que se puede recorrer con next)
        const result = yield images.toArray() // convirtiendo images en un arreglo
        console.log('DB-getImages-result: ', result)
        return Promise.resolve(result)
      })
    } catch (error) {
      console.log('db-getImages-->error: ', error)
    }
    console.log('DB-getImages-tasks', tasks)
    // resolviendo corutina
    return Promise.resolve(tasks()).asCallback(callback)
  }

  saveUser (user, callback) {
    if (!this.connected) {
      return Promise.reject(new Error('No conectado')).asCallback(callback)
    }
    // referencia de conexion y db para la corutina
    const connectionRef = this.connection
    const dbRef = this.db
    console.log('db--THIS----> ', this)
    // usando corutina(recibe un funcion constructora o generadora) de tareas
    const tasks = co.wrap(function * () {
      const conn = yield connectionRef
      user.password = utils.encrypt(user.password)
      user.createdAt = new Date()
      console.log("DB-saveUser---> ", user)
      const result = yield r.db(dbRef).table('users').insert(user).run(conn)
      if (result.errors > 0) {
        return Promise.reject(new Error(result.first_error))
      }
      console.log('se guardo')
      user.id = result.generated_keys[0]
      const created = yield r.db(dbRef).table('users').get(user.id).run(conn)
      return Promise.resolve(created)
    })
    // resolviendo corutina
    return Promise.resolve(tasks()).asCallback(callback)
  }

  getUser (username, callback) {
    if (!this.connected) {
      return Promise.reject(new Error('No conectado')).asCallback(callback)
    }
    // referencia de conexion y db para la corutina
    const connectionRef = this.connection
    const dbRef = this.db
    // usando corutina(recibe un funcion constructora o generadora) de tareas
    const tasks = co.wrap(function * () {
      const conn = yield connectionRef
      yield r.db(dbRef).table('users').indexWait().run(conn)
      const users = yield r.db(dbRef).table('users').filter({ username: username }).run(conn)
      let result = null
      try {
        result = yield users.next()
      } catch (error) {
        // return Promise.reject(new Error(`user ${username} not found`))
        return Promise.resolve(false)
      }
      return Promise.resolve(result)
    })
    // resolviendo corutina
    return Promise.resolve(tasks()).asCallback(callback)
  }

  authenticate (username, password, callback) {
    if (!this.connected) {
      return Promise.reject(new Error('No conectado')).asCallback(callback)
    }
    const getUser = this.getUser.bind(this)
    // usando corutina(recibe un funcion constructora o generadora) de tareas
    const tasks = co.wrap(function * () {
      let user = null
      try {
        user = yield getUser(username)
      } catch (error) {
        return Promise.resolve(false)
      }
      if (user.password === utils.encrypt(password)) {
        return Promise.resolve(true)
      }
      return Promise.resolve(false)
    })
    // resolviendo corutina
    return Promise.resolve(tasks()).asCallback(callback)
  }

  getImagesByUser (userId, callback) {
    if (!this.connected) {
      return Promise.reject(new Error('No conectado')).asCallback(callback)
    }

    const connectionRef = this.connection
    const dbRef = this.db
    // usando corutina(recibe un funcion constructora o generadora) de tareas
    const tasks = co.wrap(function * () {
      const conn = yield connectionRef
      // esperar a que los indices existan antes de hacer la consulta
      yield r.db(dbRef).table('imagenes').indexWait().run(conn)
      const images = yield r.db(dbRef).table('imagenes').filter({ userId: userId }).orderBy(r.desc('createdAt')).run(conn)

      const result = yield images.toArray()
      return Promise.resolve(result)
    })
    // resolviendo corutina
    return Promise.resolve(tasks()).asCallback(callback)
  }

  getImagesByTag (tag, callback) {
    if (!this.connected) {
      return Promise.reject(new Error('No conectado')).asCallback(callback)
    }

    const connectionRef = this.connection
    const dbRef = this.db
    tag = utils.normalize(tag)
    // usando corutina(recibe un funcion constructora o generadora) de tareas
    const tasks = co.wrap(function * () {
      const conn = yield connectionRef
      // esperar a que los indices existan antes de hacer la consulta
      yield r.db(dbRef).table('imagenes').indexWait().run(conn)
      const images = yield r.db(dbRef).table('imagenes').filter((img) => {
        return img('tags').contains(tag) // devolver las imagnes que en sus tags contengan el tag buscado
      }).orderBy(r.desc('createdAt')).run(conn)

      const result = yield images.toArray()
      return Promise.resolve(result)
    })
    // resolviendo corutina
    return Promise.resolve(tasks()).asCallback(callback)
  }
}

module.exports = Db
