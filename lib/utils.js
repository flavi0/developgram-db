'use strict'

const utils = {
  extractTags,
  encrypt,
  normalize
}

function extractTags (text) {
  if (text == null) return []
  let matches = text.match(/#(\w+)/g)

  if (matches == null) return []

  // ejecutar 'normalize' para cada elemento del arreglo
  matches = matches.map(normalize)

  return matches
}

function normalize (text) {
  text = text.toLowerCase()
  text = text.replace(/#/g, '')
  return text
}

function encrypt (password) {
  const shasum = require('crypto').createHash('sha256')
  shasum.update(password)
  return shasum.digest('hex')
}

module.exports = utils
